class Bank extends AbstractPlayer {

  constructor() {
    super();
  }

  addCard(card) {
    super.addCard(card);
    this._updateDeckUI();
  }

  hasWon() {
    return this.score >= Constants.BANK_MINOR_SCORE_LIMIT && this.score <= Constants.BANK_MAJOR_SCORE_LIMIT;
  }
 
  _updateDeckUI() {
    super._updateDeckUI(
      {
        deckID: Constants.BANK_DECK_ID,
        moneyID: Constants.BANK_MONEY_ID
      }
    );
  }
}