class AbstractPlayer {

  constructor() {
    this.deck = [];
    this.score = 0;
    this.money = 0;
  }

  addCard(card) {
    this.deck.push(card);
    this.incScore(card.value);
  }

  incScore(score) {
    this.score += score;
  }

  setScore(score) {
    this.score = score;
  }

  _updateDeckUI({deckID, scoreID, moneyID}) {
    // DECK
    let deckDOM = document.querySelector(Constants.ID_PREFIX + deckID);
    deckDOM.innerHTML = "";
    this.deck.forEach(card => {
      let newDOMImg = document.createElement('img');
      newDOMImg.src = card.getAsset();
      deckDOM.appendChild(newDOMImg);
    });

    // SCORE
    if (scoreID) {
      let scoreDOM = document.querySelector(Constants.ID_PREFIX + scoreID);
      scoreDOM.innerHTML = this.score;
    }

    // MONEY
    let moneyDOM = document.querySelector(Constants.ID_PREFIX + moneyID);
    moneyDOM.innerHTML = this.money;
  }

}