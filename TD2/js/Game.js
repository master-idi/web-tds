class Game {

  constructor() {
    this.onClickNewPlayerCardButton = this.onClickNewPlayerCardButton.bind(this);
    this.onClickResetGameButton = this.onClickResetGameButton.bind(this);
    this.onClickNextPlayerButton = this.onClickNextPlayerButton.bind(this);
    this._initGame = this._initGame.bind(this);
  }

  onClickNewPlayerCardButton() {
    const newCard = this._getRandomCard();
    this.player.addCard(newCard);

    if (this.player.hasLost()) {
      Utils.displayModal({
        onCloseCallback: this._initGame,
        cssClass: "danger",
        title: "Vous avez perdu !"
      });
    }

    if (this.player.hasWon()) {
      Utils.displayModal({
        onCloseCallback: this._initGame,
        cssClass: 'success',
        title: "Vous avez gagné !"
      });
    }
  }

  onClickNextPlayerButton() {
    this._doBankPoint();
  }

  onClickResetGameButton() {
    this._initGame();
  }

  run() {
    this._initGame();
    this._initUIButtons();
  }

  _initGame() {
    this.cards = [];
    this.bank = new Bank();
    this.player = new Player();
    this._initCardsValue();
    this.bank.addCard(this._getRandomCard());
    this.player.addCard(this._getRandomCard());
  }

  _initUIButtons() {
    let newCardButton = document.querySelector('#new-card-button');
    newCardButton.addEventListener("click", this.onClickNewPlayerCardButton);

    let nextPlayerButton = document.querySelector('#next-player-button');
    nextPlayerButton.addEventListener('click', this.onClickNextPlayerButton);

    let resetGameButton = document.querySelector('#reset-button');
    resetGameButton.addEventListener('click', this.onClickResetGameButton);
  }

  _initCardsValue() {
    cardsData.forEach(data => {
      this.cards.push(new Card(data.code));
    });
  }

  _doBankPoint() {
    while (this.bank.score < Constants.BANK_MINOR_SCORE_LIMIT) {
      this.bank.addCard(this._getRandomCard());
    }

    if(this.bank.hasWon()) {
      Utils.displayModal({
        onCloseCallback: this._initGame,
        cssClass: "danger",
        title: "Vous avez perdu !"
      });

    } else {
      Utils.displayModal({
        onCloseCallback: this._initGame,
        cssClass: 'success',
        title: "Vous avez gagné !"
      });
    }
  }

  _getRandomCard() {
    return this.cards[Math.floor(Math.random() * (this.cards.length - 0))];
  }
}