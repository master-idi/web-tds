class Utils {

  constructor() {

  }

  static displayModal(options) {
    let enableBackdropDismiss = options.enableBackdropDismiss || false;
    let onCloseCallback = options.onCloseCallback;
    let cssClass = options.cssClass || "primary";
    let title = options.title || "";
    let content = options.content || "";

    let modal = document.querySelector("#modal");
    let modalTitle = document.querySelector("#modal-title");
    let modalContent = document.querySelector("#modal-content");
    let modalHeader = document.getElementsByClassName("modal-header")[0];
    let span = document.getElementsByClassName("close")[0];

    modal.style.display = "block";
    modalHeader.classList.add(cssClass);

    if (modalTitle) {
      modalTitle.innerHTML = title;
    }

    if (modalContent) {
      modalContent.innerHTML = content;
    }

    span.onclick = function () {
      modal.style.display = "none";
      onCloseCallback();
    }

    if (enableBackdropDismiss) (
      window.onclick = function (event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
    )
  }
}