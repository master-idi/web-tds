const Constants = {
  CARDS_ASSETS_PATH: "./assets/cards/",
  ASSET_EXT: ".png",
  FIGURE_VALUE: 10,
  AS_VALUE: 1,
  PLAYER_SCORE_LIMIT: 21,
  BANK_MINOR_SCORE_LIMIT: 17,
  BANK_MAJOR_SCORE_LIMIT: 21,
  ID_PREFIX: '#',
  // BANK IDs
  BANK_DECK_ID: "bank-deck",
  BANK_MONEY_ID: "bank-money",
  // PLAYER IDs
  MY_DECK_ID: "my-deck",
  MY_SCORE_ID: "my-score",
  MY_MONEY_ID: "my-money"
}