class Card {

  constructor(code) {
    this.code = code;
    this.value = this.getValue(this.code);
  }

  getAsset() {
    return Constants.CARDS_ASSETS_PATH + this.code + Constants.ASSET_EXT;
  }

  getFigureValue(cardCode) {
    return cardCode.match(/^[^A\d][B-Z]$/g);
  }

  getASValue(cardCode) {
    return cardCode.match(/^A[A-Z]$/g);
  }

  getPointValue(cardCode) {
    return Number(cardCode.match(/\d+/g)[0]);
  }

  getValue(cardCode) {
    if (this.getFigureValue(cardCode))
      return Constants.FIGURE_VALUE;
    else if (this.getASValue(cardCode))
      return Constants.AS_VALUE;
    else
      return this.getPointValue(cardCode);
  }

}