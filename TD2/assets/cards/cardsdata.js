cardsData = [
  {
    "code": "10C"
  },
  {
    "code": "10D"
  },
  {
    "code": "10H"
  },
  {
    "code": "10S"
  },
  {
    "code": "2C"
  },
  {
    "code": "2D"
  },
  {
    "code": "2H"
  },
  {
    "code": "2S"
  },
  {
    "code": "3C"
  },
  {
    "code": "3D"
  },
  {
    "code": "3H"
  },
  {
    "code": "3S"
  },
  {
    "code": "4C"
  },
  {
    "code": "4D"
  },
  {
    "code": "4H"
  },
  {
    "code": "4S"
  },
  {
    "code": "5C"
  },
  {
    "code": "5D"
  },
  {
    "code": "5H"
  },
  {
    "code": "5S"
  },
  {
    "code": "6C"
  },
  {
    "code": "6D"
  },
  {
    "code": "6H"
  },
  {
    "code": "6S"
  },
  {
    "code": "7C"
  },
  {
    "code": "7D"
  },
  {
    "code": "7H"
  },
  {
    "code": "7S"
  },
  {
    "code": "8C"
  },
  {
    "code": "8D"
  },
  {
    "code": "8H"
  },
  {
    "code": "8S"
  },
  {
    "code": "9C"
  },
  {
    "code": "9D"
  },
  {
    "code": "9H"
  },
  {
    "code": "9S"
  },
  {
    "code": "AC"
  },
  {
    "code": "AD"
  },
  {
    "code": "AH"
  },
  {
    "code": "AS"
  },
  {
    "code": "JC"
  },
  {
    "code": "JD"
  },
  {
    "code": "JH"
  },
  {
    "code": "JS"
  },
  {
    "code": "KC"
  },
  {
    "code": "KD"
  },
  {
    "code": "KH"
  },
  {
    "code": "KS"
  },
  {
    "code": "QC"
  },
  {
    "code": "QD"
  },
  {
    "code": "QH"
  },
  {
    "code": "QS"
  }
]
