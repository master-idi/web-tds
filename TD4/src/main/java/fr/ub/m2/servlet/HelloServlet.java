package fr.ub.m2.servlet;

import java.io.*;
import java.util.Enumeration;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(urlPatterns="/world")
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.printHelloWithParam(req, resp);
        //this.printHeaders(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");
        String name = req.getParameter("name");
        PrintWriter out = resp.getWriter();
        out.println("Hello " + name);
        req.getSession().setAttribute("name", name);

        //this.printHeaders(req, resp);
    }

    private void printHelloWithParam(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        String name = req.getParameter("name");

        if (name == null || name.isEmpty() || name.isBlank()) {
            name = (String) req.getSession().getAttribute("name");
        }

        out.println("<html>");
        out.println("<body>");
        out.println("Hello " + name);
        out.println("</body>");
        out.println("</html>");
    }

    private void printHeaders(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<head><link rel=\"stylesheet\" href=\"./style.css\"/></head>");
        out.println("<b>Request Method: </b>" + req.getMethod() + "<br/>");
        out.println("<b>Request URI: </b>" + req.getRequestURI() + "<br/>");
        out.println("<b>Request Protocol: </b>" + req.getProtocol() + "<br/><br/>");
        out.println("<table><thead>\n");
        out.println("\t<tr>\n<th>Header Name</th><th>Header Value</th></tr>\n");
        out.println("</thead><tbody>");
        Enumeration headerNames = req.getHeaderNames();
        while(headerNames.hasMoreElements()) {
            String headerName = (String)headerNames.nextElement();
            out.println("\t<tr>\n\t\t<td>" + headerName + "</td>");
            out.println("<td>" + req.getHeader(headerName) + "</td>\n");
            out.println("\t</tr>\n");
        }
        out.println("</tbody></table>");
    }
}