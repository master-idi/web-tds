const Constants = {
  WEATHER_API_BASE_URL: 'https://www.prevision-meteo.ch/services/json/',
  MAP_CONFIG: {
    LAYER_URL: 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
    OPTS: {
      MAX_ZOOM: 18,
      ID: 'mapbox/streets-v11',
      TILE_SIZE: 512,
      ZOOM_OFFSET: -1
    }
  },
  TOWNS_API_BASE_URL: 'https://geo.api.gouv.fr/communes?',
  TOWNS_API_GEO_JSON_SUFFIX: '&format=geojson',
  ZIP_CODE_REGEX: /\d{5}/,
  ERRORS: {
    CITY_NOT_FOUND: 1,
    UNABLE_TO_FETCH_WEATHER: 2
  }
};