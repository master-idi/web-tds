class Utils {

  constructor() {
  }

  static normalizeString(str) {
    return str
      .toLowerCase()
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "");
  }

  static displayAlertMessage(text, cssClass) {
    const alertMessageRow = document.querySelector('#alert-message-row');
    const alertMessage = alertMessageRow.querySelector('#alert-message');
    alertMessage.innerHTML = text;
    alertMessage.classList.add(cssClass);
    alertMessageRow.classList.remove('d-none');
  }

  static removeAlertMessage() {
    const alertMessageRow = document.querySelector('#alert-message-row');
    const alertMessage = alertMessageRow.querySelector('#alert-message');
    alertMessage.innerHTML = '';
    alertMessageRow.classList.add('d-none');
  }
}