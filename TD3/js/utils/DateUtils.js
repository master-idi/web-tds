class DateUtils {

  constructor() {
  }

  static getLocaleDateString(date) {
    const options = { year: 'numeric', month: 'short', day: 'numeric' };
    return date.toLocaleDateString(navigator.language, options);
  }

  static getNumericDayAndMonth(date) {
    const options = { month: 'numeric', day: 'numeric' };
    return date.toLocaleDateString(navigator.language, options);
  }

  static getHourToStringFromDate(date) {
    return `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;
  }

  static getDateFromString(dateToParse, hour) {
    const splittedDate = dateToParse.split('.');
    const date = new Date(splittedDate[2], Number(splittedDate[1]) - 1, splittedDate[0]);
    if (hour) {
      let splittedHour;
      let separator;

      if (hour.toLowerCase().includes(':'))
        separator = ':';
      else if (hour.toLowerCase().includes('h'))
        separator = 'h';
      else
        throw `Invalid time format: ${hour}`;

      splittedHour = hour.toLowerCase().split(separator);
      date.setHours(splittedHour[0], splittedHour[1]);
    }
    return date;
  }
}