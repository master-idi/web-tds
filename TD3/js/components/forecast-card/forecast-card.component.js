class ForecastCardComponent extends HTMLElement {

  constructor(forecastInfo) {
    super();
    this._setProperties(forecastInfo);
    this.innerHTML = this._render();
    this.classList.add('col-12');
    this.classList.add('col-sm-6');
    this.classList.add('col-md-6');
    this.classList.add('col-lg-4');
  }

  _render() {
    return `
      <div class="card shadow rounded my-4">
        <div class="card-body">
          <img class="weather-icon" src="${this.icon}" alt="">

          <!--  GENERAL ROW --> 
          <div id="general-row" class="row">
            <div class="col">
              <p class="date">
                ${DateUtils.getLocaleDateString(this.date)}
              </p>
            </div>
          </div>

          <!--  CONDITION ROW --> 
          <div id="condition-row" class="row my-3">
            <div class="col">
              <h1 class="weather-temp card-title">
                ${this.minTemp}°/${this.maxTemp}°
              </h1>
              <p>
                ${this.condition}
              </p>
            </div>
          </div>

          <!--  HOURLY DATA ROW --> 
          <div id="hourly-data-row" class="row flex-nowrap overflow-auto">
            ${this._renderHourlyData()}
          </div>
        </div>
      </div>
    `;
  }

  _setProperties(forecastInfo) {
    this.date = forecastInfo.date;
    this.minTemp = forecastInfo.minTemp;
    this.maxTemp = forecastInfo.maxTemp;
    this.condition = forecastInfo.condition;
    this.icon = forecastInfo.iconBig;
    this.hourlyData = forecastInfo.hourlyData;
  }

  _renderHourlyData() {
    let html = '';
    this.hourlyData.forEach(data => {
      html += `
        <div class="col text-center">
          <p>${DateUtils.getHourToStringFromDate(data.date)}</p>
          <img src="${data.icon}" alt="">
          <p>${data.temp}°</p>
        </div>
    `;
    });

    return html;
  }


}
customElements.define('app-forecast-card', ForecastCardComponent);