class WeatherCardComponent extends HTMLElement {

  constructor(weatherInfo) {
    super();
    this._setProperties(weatherInfo);
    this.innerHTML = this._render();
  }

  _render() {
    return `
      <div class="card shadow rounded my-4">
        <div class="card-body">
          <img class="weather-icon" src="${this.iconBig}" alt="">

          <!--  GENERAL ROW --> 
          <div id="general-row" class="row">
            <div class="col">
              <p>
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-geo-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M4 4a4 4 0 1 1 4.5 3.969V13.5a.5.5 0 0 1-1 0V7.97A4 4 0 0 1 4 3.999zm2.493 8.574a.5.5 0 0 1-.411.575c-.712.118-1.28.295-1.655.493a1.319 1.319 0 0 0-.37.265.301.301 0 0 0-.057.09V14l.002.008a.147.147 0 0 0 .016.033.617.617 0 0 0 .145.15c.165.13.435.27.813.395.751.25 1.82.414 3.024.414s2.273-.163 3.024-.414c.378-.126.648-.265.813-.395a.619.619 0 0 0 .146-.15.148.148 0 0 0 .015-.033L12 14v-.004a.301.301 0 0 0-.057-.09 1.318 1.318 0 0 0-.37-.264c-.376-.198-.943-.375-1.655-.493a.5.5 0 1 1 .164-.986c.77.127 1.452.328 1.957.594C12.5 13 13 13.4 13 14c0 .426-.26.752-.544.977-.29.228-.68.413-1.116.558-.878.293-2.059.465-3.34.465-1.281 0-2.462-.172-3.34-.465-.436-.145-.826-.33-1.116-.558C3.26 14.752 3 14.426 3 14c0-.599.5-1 .961-1.243.505-.266 1.187-.467 1.957-.594a.5.5 0 0 1 .575.411z"/>
                </svg>
                ${this.cityName}, ${this.cityCountry}
              </p>
              <p class="date">
                ${DateUtils.getLocaleDateString(this.date)}, ${DateUtils.getHourToStringFromDate(this.date)}
              </p>
            </div>
          </div>

          <!--  CONDITION ROW --> 
          <div id="condition-row" class="row mt-3">
            <div class="col">
              <h1 class="weather-temp card-title">
                ${this.currentTemp}°
              </h1>
              <p>
                ${this.currentCondition}
              </p>
            </div>
          </div>

          <!--  CONDITION DETAILS ROW --> 
          <div id="condition-details-row" class="row py-3">
            <div class="col-6 text-center pb-2">
              <p>
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-droplet-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M8 16a6 6 0 0 0 6-6c0-1.655-1.122-2.904-2.432-4.362C10.254 4.176 8.75 2.503 8 0c0 0-6 5.686-6 10a6 6 0 0 0 6 6zM6.646 4.646c-.376.377-1.272 1.489-2.093 3.13l.894.448c.78-1.559 1.616-2.58 1.907-2.87l-.708-.708z"/>
                </svg>
                &nbsp;${this.humidity} %
              </p>
            </div>
            <div class="col-6 text-center pb-2">
              <p>
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-list-nested" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M4.5 11.5A.5.5 0 0 1 5 11h10a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zm-2-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm-2-4A.5.5 0 0 1 1 3h10a.5.5 0 0 1 0 1H1a.5.5 0 0 1-.5-.5z"/>
                </svg>
                &nbsp;${this.windSpeed} km/h
              </p>
            </div>
            <div class="col-6 text-center pb-2">
              <p>
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-brightness-alt-high-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M4 11a4 4 0 1 1 8 0 .5.5 0 0 1-.5.5h-7A.5.5 0 0 1 4 11zm4-8a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 3zm8 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zM3 11a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5zm10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.414a.5.5 0 1 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0zM4.464 7.464a.5.5 0 0 1-.707 0L2.343 6.05a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707z"/>
                </svg>
                &nbsp;${this.sunrise}
              </p>
            </div>
            <div class="col-6 text-center pb-2">
              <p>
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-brightness-alt-low-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path d="M8.5 5.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0zm5 6a.5.5 0 1 1 0-1 .5.5 0 0 1 0 1zm-11 0a.5.5 0 1 1 0-1 .5.5 0 0 1 0 1zm9.743-4.036a.5.5 0 1 1-.707-.707.5.5 0 0 1 .707.707zm-8.486 0a.5.5 0 1 1 .707-.707.5.5 0 0 1-.707.707z"/>
                  <path fill-rule="evenodd" d="M4 11a4 4 0 1 1 8 0 .5.5 0 0 1-.5.5h-7A.5.5 0 0 1 4 11z"/>
                </svg>
                &nbsp;${this.sunset}
              </p>
            </div>
          </div>

          <!--  HOURLY DATA ROW --> 
          <div id="hourly-data-row" class="row flex-nowrap overflow-auto">
            ${this._renderHourlyData()}
          </div>
        </div>
      </div>
    `;
  }

  _setProperties(weatherInfo) {
    this.cityName = weatherInfo.cityInfo.name;
    this.cityCountry = weatherInfo.cityInfo.country;
    this.date = weatherInfo.currentCondition.date;
    this.currentTemp = weatherInfo.currentCondition.temp;
    this.currentCondition = weatherInfo.currentCondition.condition;
    this.humidity = weatherInfo.currentCondition.humidity;
    this.iconBig = weatherInfo.currentCondition.iconBig;
    this.fcstDayZero = weatherInfo.currentCondition.fcstDayZero;
    this.sunrise = weatherInfo.cityInfo.sunrise;
    this.sunset = weatherInfo.cityInfo.sunset;
    this.windSpeed = weatherInfo.currentCondition.windSpeed;
  }

  _renderHourlyData() {
    let html = '';
    this.fcstDayZero.hourlyData.forEach(data => {
      html += `
        <div class="col text-center">
          <p>${DateUtils.getHourToStringFromDate(data.date)}</p>
          <img src="${data.icon}" alt="">
          <p>${data.temp}°</p>
        </div>
    `;
    });

    return html;
  }


}
customElements.define('app-weather-card', WeatherCardComponent);