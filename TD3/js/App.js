class App {

  constructor() {
    this.mapService = new MapService();
    this.weatherService = new WeatherService();
    this.townsService = new TownsService();

    this._onSubmitSearchForm = this._onSubmitSearchForm.bind(this);
    this._getWeatherByLatLng = this._getWeatherByLatLng.bind(this);
    this._addCardToCurrentWeatherContainer = this._addCardToCurrentWeatherContainer.bind(this);
    this._getWeatherByCityName = this._getWeatherByCityName.bind(this);
    this._addTownsMarkersToMap = this._addTownsMarkersToMap.bind(this);
  }

  startApp() {
    this._initEvents();
  }

  _initEvents() {
    document.querySelector('#searchFormValue').addEventListener('change', () => {
      this._removeSelectFilledWithTowns();
    });

    const searchForm = document.querySelector('#search-form');
    searchForm.addEventListener('submit', (e) => {
      this._resetUI();
      e.preventDefault();
      this._onSubmitSearchForm();
    });

    const townsSelect = document.querySelector('#towns-select');
    townsSelect.addEventListener('change', (e) => {
      this._onChangeTownOption(e.target.value);
    })

    this.mapService.setOnClickMapListener((latlng) => {
      this._resetUI();
      this._removeSelectFilledWithTowns();
      this._getWeatherByLatLng(latlng)
        .then(weatherInfo => {
          this._addCardToCurrentWeatherContainer(weatherInfo);
          document.querySelector('#current-card-container').scrollIntoView({ behavior: 'smooth' });
        })
    });
  }

  _onSubmitSearchForm() {
    const searchValue = document.querySelector('#searchFormValue').value;
    if (this._isZipCode(searchValue)) {
      this._searchByZipCode(searchValue);
    } else {
      this._searchByName(searchValue);
    }
  }

  _onClickTownMarker(town) {
    this._resetUI();
    this.townsService.getTownsByName(town.name)
      .then(towns => {
        if (towns.length > 1) {
          this._getWeatherByCityName(`${Utils.normalizeString(town.name)}-${town.departmentCode}`)
            .then(weatherInfo => {
              this._addCardToCurrentWeatherContainer(weatherInfo);
            });
        } else {
          this._getWeatherByCityName(town.name)
            .then(weatherInfo => {
              this._addCardToCurrentWeatherContainer(weatherInfo);
            });
        }
        document.querySelector('#current-card-container').scrollIntoView({ behavior: 'smooth' });
      })
      .catch(error => console.error(error));
  }

  _onChangeTownOption(value) {
    // Workaround for identical towns name with prevision-meteo.ch
    const normalizedValue = Utils.normalizeString(value);
    // end of workaround
    this._getWeatherByCityName(normalizedValue)
      .then(weatherInfo => {
        this._addCardToCurrentWeatherContainer(weatherInfo);
      });
  }

  _searchByZipCode(searchValue) {
    this._getTownsByZipCode(searchValue)
      .then(towns => {
        if (!towns || !Array.isArray(towns) || towns.length === 0)
          return Promise.reject(towns);
        this._addTownsMarkersToMap(towns);
        document.querySelector('#mapid').scrollIntoView({ behavior: 'smooth' });
      });
  }

  _searchByName(searchValue) {
    this.townsService.getTownsByName(searchValue)
      .then(towns => {
        console.log(towns);
        if (!towns || !Array.isArray(towns) || towns.length === 0)
          return Promise.reject(towns);

        if (towns.length > 1) {
          this._displaySelectFilledWithTowns(towns);
        } else {
          this._getWeatherByCityName(towns[0].name)
            .then(weatherInfo => {
              this._addTownMarkerToMap(weatherInfo);
            });
        }
      })
      .catch(error => {
        console.error(error);
        this._handleError(error);
      });
  }

  async _getWeatherByLatLng(latlng) {
    return this.weatherService.getWeatherByLatAndLng(latlng.lat, latlng.lng)
      .then(weatherInfo => weatherInfo)
      .catch(error => {
        this._handleError(error);
      });
  }

  async _getWeatherByCityName(cityName) {
    return this.weatherService.getWeatherByLocationName(cityName)
      .then(weatherInfo => weatherInfo)
      .catch(error => {
        console.error(error);
        this._handleError(error);
      });
  }

  async _getTownsByZipCode(searchValue) {
    return this.townsService.getTownsByZipCode(searchValue)
      .then(towns => towns)
      .catch(error => {
        console.error(error);
        this._handleError(error);
      });
  }

  _addTownMarkerToMap(weatherInfo) {
    this._addCardToCurrentWeatherContainer(weatherInfo);
    const lat = weatherInfo.cityInfo.lat;
    const lng = weatherInfo.cityInfo.lng;
    this.mapService.addMarker(lat, lng)
    this.mapService.flyToCoordinates([lat], [lng]);
  }

  _addTownsMarkersToMap(towns) {
    const dataKeys = { lat: 'lat', lng: 'lng', title: 'name' };
    this.mapService.addMarkers(towns, dataKeys, (town) => this._onClickTownMarker(town));
    const latArray = towns.map(town => town.lat);
    const lngArray = towns.map(town => town.lng);
    this.mapService.flyToCoordinates(latArray, lngArray);
  }

  _addCardToCurrentWeatherContainer(weatherInfo) {
    const currentWeatherCardContainer = document.querySelector('#current-card-container');
    currentWeatherCardContainer.innerHTML = '';
    currentWeatherCardContainer.append(new WeatherCardComponent(weatherInfo));

    const forecastCardsContainer = document.querySelector('#forecast-cards-container');
    forecastCardsContainer.innerHTML = '';
    weatherInfo.forecastDays.forEach(day => {
      forecastCardsContainer.append(new ForecastCardComponent(day));
    })
  }

  _displaySelectFilledWithTowns(towns) {
    const townsSelect = document.querySelector('#towns-select');
    townsSelect.innerHTML = '';
    let townOption = document.createElement('option');
    townOption.value = `null`;
    townOption.text = `-- Sélectionner une ville --`;
    townsSelect.appendChild(townOption);
    towns.forEach(town => {
      townOption = document.createElement('option');
      townOption.value = `${town.name}-${town.departmentCode}`;
      townOption.text = `${town.name} (${town.departmentCode})`;
      townsSelect.appendChild(townOption);
    });
    townsSelect.classList.remove('towns-select-hidden');
  }

  _removeSelectFilledWithTowns() {
    const townsSelect = document.querySelector('#towns-select');
    townsSelect.innerHTML = '';
    townsSelect.classList.add('towns-select-hidden');
  }

  _isZipCode(searchValue) {
    return searchValue.match(Constants.ZIP_CODE_REGEX);
  }

  _handleError(errorCode) {
    let textError = '';
    switch (errorCode) {
      case Constants.ERRORS.CITY_NOT_FOUND:
        textError = 'Nom de ville ou code postal invalide/introuvable.';
        break;
      case Constants.ERRORS.UNABLE_TO_FETCH_WEATHER:
        textError = 'Une erreur est survenue lors de la récupération des données météo.';
        break;
      default:
        textError = 'Une erreur est survenue';
        break;
    }
    Utils.displayAlertMessage(textError, 'alert-danger');
  }

  _resetUI() {
    document.querySelector('#current-card-container').innerHTML = '';
    document.querySelector('#forecast-cards-container').innerHTML = '';
    Utils.removeAlertMessage();
  }
}