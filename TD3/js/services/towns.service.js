class TownsService {

  constructor() {

  }

  async getTownsByZipCode(zipCode) {
    return fetch(Constants.TOWNS_API_BASE_URL + `codePostal=${zipCode}` + Constants.TOWNS_API_GEO_JSON_SUFFIX)
      .then(response => response.json())
      .catch(error => Promise.reject(error))
      .then(json => {
        console.log(json);
        const towns = [];
        if (Array.isArray(json.features) && json.features.length > 0)
          json.features.forEach(feature => towns.push(new TownInfo(feature)));
        else
          return Promise.reject(Constants.ERRORS.CITY_NOT_FOUND);

        return towns;
      })
      .catch(error => Promise.reject(error));
  }

  async getTownsByName(name) {
    return fetch(Constants.TOWNS_API_BASE_URL + `nom=${name}` + Constants.TOWNS_API_GEO_JSON_SUFFIX)
      .then(response => response.json())
      .catch(error => Promise.reject(error))
      .then(json => {
        console.log(json);
        const towns = [];
        if (Array.isArray(json.features) && json.features.length > 0) {
          json.features.forEach(feature => towns.push(new TownInfo(feature)));
          return towns.filter(town => Utils.normalizeString(town.name) === Utils.normalizeString(name));
        } else {
          return Promise.reject(Constants.ERRORS.CITY_NOT_FOUND);
        }
      })
      .catch(error => Promise.reject(error));
  }
}