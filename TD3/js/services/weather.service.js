class WeatherService {

  constructor() { }

  async getWeatherByLocationName(locationName) {
    return this._fetchFromApiToJson(locationName);
  }

  async getWeatherByLatAndLng(lat, lng) {
    const formattedQuery = "lat=" + lat + "lng=" + lng;
    return this._fetchFromApiToJson(formattedQuery);
  }

  async _fetchFromApiToJson(queryValue) {

    return fetch(Constants.WEATHER_API_BASE_URL + queryValue)
      .then(response => response.json())
      .catch(error => Promise.reject(error))
      .then(jsonResponse => {
        if (Array.isArray(jsonResponse.errors) && jsonResponse.errors.length > 0)
          return Promise.reject(Constants.ERRORS.UNABLE_TO_FETCH_WEATHER);
        return new WeatherInfo(jsonResponse);
      })
      .catch(error => Promise.reject(error));
  }
}