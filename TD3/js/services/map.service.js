class MapService {

  constructor() {
    this.map = null;
    this.marker = null;
    this.markers = null;
    this._clickMapCallback = null;
    this.townsMarkersMap = new Map();

    this._initMap();
    this._removeMarker = this._removeMarker.bind(this);
  }

  setOnClickMapListener(callback) {
    this._clickMapCallback = callback;
  }

  addMarker(lat, lng, markerClickCallback) {
    if (this.marker) {
      this.map.removeLayer(this.marker);
    }
    this.marker = new L.marker([lat, lng]);
    this.marker.addTo(this.map);
    this.marker.on('click', (e) => {
      markerClickCallback(e);
    });
  }

  addMarkers(list, dataKeys, clickCallback) {
    if (!dataKeys && !dataKeys.lat && !dataKeys.lng)
      return;

    if (!list)
      return;

    if (this.markers) {
      this.markers.clearLayers();
    }

    if (this.marker) {
      this.map.removeLayer(this.marker);
    }

    this.markers = L.layerGroup();
    list.forEach(item => {
      const marker = L.marker([item[dataKeys.lat], item[dataKeys.lng]], { title: item[dataKeys.title] });
      marker.on('click', () => clickCallback(item));
      this.markers.addLayer(marker);
    });
    this.map.addLayer(this.markers);
  }

  flyToCoordinates(latArray, lngArray) {
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    const averageLat = latArray.reduce(reducer) / latArray.length;
    const averageLng = lngArray.reduce(reducer) / lngArray.length;
    this.map.flyTo([averageLat, averageLng], 11);
  }

  _initMap() {
    const mapOptions = {
      maxZoom: Constants.MAP_CONFIG.OPTS.MAX_ZOOM,
      id: Constants.MAP_CONFIG.OPTS.ID,
      tileSize: Constants.MAP_CONFIG.OPTS.TILE_SIZE,
      zoomOffset: Constants.MAP_CONFIG.OPTS.ZOOM_OFFSET
    }

    this.map = L.map('mapid');
    this.map.setView([47, 2], 5);

    const tileLayer = L.tileLayer(
      Constants.MAP_CONFIG.LAYER_URL,
      mapOptions
    )
    tileLayer.addTo(this.map);

    this.map.on('click', (e) => {
      this.addMarker(e.latlng.lat, e.latlng.lng, this._removeMarker);
      if (this._clickMapCallback) {
        this._clickMapCallback(this.marker.getLatLng());
      }
    }, this);
  }

  _removeMarker(e) {
    this.map.removeLayer(e.target);
  }
}