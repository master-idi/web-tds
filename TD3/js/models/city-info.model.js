class CityInfo {

  constructor(jsonCityInfo) {
    this.country = jsonCityInfo.country;
    this.elevation = jsonCityInfo.elevation;
    this.lat = jsonCityInfo.latitude;
    this.lng = jsonCityInfo.longitude;
    this.name = jsonCityInfo.name;
    this.sunrise = jsonCityInfo.sunrise;
    this.sunset = jsonCityInfo.sunset
  }
}