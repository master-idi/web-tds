class HourlyInfo {

  constructor(jsonHour, jsonHourlyData, date) {
    this.date = DateUtils.getDateFromString(date, jsonHour);
    this.icon = jsonHourlyData.ICON;
    this.temp = Math.round(jsonHourlyData.TMP2m);
  }
}