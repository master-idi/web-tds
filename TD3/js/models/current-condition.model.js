class CurrentCondition {

  constructor(json) {
    const jsonCurrentCondition = json.current_condition;

    this.date = DateUtils.getDateFromString(jsonCurrentCondition.date, jsonCurrentCondition.hour);
    this.temp = jsonCurrentCondition.tmp;
    this.windSpeed = jsonCurrentCondition.wnd_spd;
    this.windGust = jsonCurrentCondition.wnd_gust;
    this.windDirection = jsonCurrentCondition.wnd_dir;
    this.pressure = jsonCurrentCondition.pressure;
    this.humidity = jsonCurrentCondition.humidity;
    this.condition = jsonCurrentCondition.condition;
    this.conditionKey = jsonCurrentCondition.condition_key;
    this.icon = jsonCurrentCondition.icon;
    this.iconBig = jsonCurrentCondition.icon_big;
    this.fcstDayZero = new ForecastDayInfo(json.fcst_day_0, this.date);
  }
}