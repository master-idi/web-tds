class WeatherInfo {  
  
  constructor(json) {
    this.cityInfo = new CityInfo(json.city_info);
    this.currentCondition = new CurrentCondition(json);
    this.forecastDays = [
      new ForecastDayInfo(json.fcst_day_1),
      new ForecastDayInfo(json.fcst_day_2),
      new ForecastDayInfo(json.fcst_day_3),
      new ForecastDayInfo(json.fcst_day_4),
    ];
  }
}