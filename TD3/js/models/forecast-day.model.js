class ForecastDayInfo {

  constructor(jsonForecastDay, date) {
    this.condition = jsonForecastDay.condition;
    this.conditionKey = jsonForecastDay.condition_key;
    this.date = date || DateUtils.getDateFromString(jsonForecastDay.date);
    this.dayLong = jsonForecastDay.day_long;
    this.dayShort = jsonForecastDay.day_short;
    this.hourlyData = this._getHourlyData(jsonForecastDay.hourly_data, jsonForecastDay.date);
    this.icon = jsonForecastDay.icon;
    this.iconBig = jsonForecastDay.icon_big;
    this.maxTemp = jsonForecastDay.tmax;
    this.minTemp = jsonForecastDay.tmin;
  }

  _getHourlyData(jsonHourlyData, date) {
    return Object.entries(jsonHourlyData)
      .map(([key, value]) => new HourlyInfo(key, value, date))
      .filter((info, index, hourlyData) => {
        return info.date.getHours() >= this.date.getHours() || index >= (hourlyData.length - 4); 
      });
  }
}