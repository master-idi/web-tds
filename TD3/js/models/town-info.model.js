class TownInfo {

  constructor(json) {
    const properties = json.properties;
    const geometry = json.geometry;

    this.name = properties.nom;
    this.code = properties.code;
    this.departmentCode = properties.codeDepartement;
    this.regionCode = properties.codeRegion;
    this.zipCodes = properties.codesPostaux;
    this.lat = geometry.coordinates[1] || '';
    this.lng = geometry.coordinates[0] || '';
  }
}